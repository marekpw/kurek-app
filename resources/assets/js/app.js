window._ = require('lodash');
window.$ = window.jQuery = require('jquery');

require('materialize-css/bin/materialize');

import './components/dropdowns';
import './components/submit-loader';