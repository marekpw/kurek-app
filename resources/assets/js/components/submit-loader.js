
let $ = require('jquery');

$('[data-toggle=submit-loader]').each(function() {
    let button = this;
    let form = $(button).closest('form');

    form.on('submit', function() {
        $(button).prop('disabled', true);
        $(button).text($(button).data('text') || 'Loading...');
    });
});