<div class="file-field input-field">
    <div class="btn waves-effect waves-light deep-orange">
        <span><i class="material-icons">file_upload</i></span>
        <input name="{{ $name }}" type="file" multiple>
    </div>
    <div class="file-path-wrapper white-text">
        <input
            class="file-path validate grey-text text-darken-1"
            disabled
            placeholder="{{ $description }}"
            type="text"
            @if($multiple) multiple @endif
            @if($required) required @endif
        >
    </div>
</div>