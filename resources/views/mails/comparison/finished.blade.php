@component('mail::message')
# Introduction

Dobrý den,

výsledky porovnání CSV souborů jsou k dispozici. Klepněte na tlačítko níže.

@component('mail::button', ['url' => $url])
Zobrazit výsledky
@endcomponent

Děkujeme za využívání našich služeb<br>
{{ config('app.name') }}
@endcomponent