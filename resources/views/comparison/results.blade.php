@extends('layout')

@section('main-title', 'Výsledky srovnání')

@section('content')
    <p>Váš požadavek na porovnání souborů byl dokončen.</p>

    <a href="{{ route('comparison.form') }}" class="btn btn-large white grey-text text-darken-3">Jít zpět</a>

    <a href="{{ url($file) }}" class="waves-effect waves-light btn btn-large deep-orange">
        <i class="material-icons left">file_download</i>
        Stáhnout výsledky
    </a>
@stop