@extends('layout')

@section('main-title', 'Požadavek vytvořen')

@section('content')
    <p>Přijali jsme váš požadavek a byl zařazen do fronty zpracování. Po dokončení budete informováni e-mailem.</p>
    <p>Děkujeme za využití našich služeb.</p>

    <a href="{{ route('comparison.form') }}" class="waves-effect waves-light btn btn-large deep-orange">Jít zpět</a>
@stop