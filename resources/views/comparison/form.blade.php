@extends('layout')

@section('main-title', 'Porovnání CSV')
@section('description', 'Nahrajte soubory dodavatelů a jednoduše porovnejte jejich ceny.')

@section('content')

    <p class="center grey-text text-darken-1">
        <strong>Poznámka:</strong>
        Do jednoho balíčku (boxu) můžete nahrát více souborů v případě, že je zdroj dodavatele rozdělen na více částí.
    </p>

    <form action="{{ route('comparison.submit') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="row">
            @for($i = 1; $i <= 3; $i++)
                <div class="col s4">
                    <div class="csv-upload-card card white darken-1">
                        <div class="card-content brown darken-2 white-text filename-section">
                            <div class="input-field">
                                <input id="package_name_{{ $i }}" name="package_names[{{ $i }}]" type="text" value="Dodavatel {{ $i }}">
                                <label class="brown-text text-lighten-4" for="package_name_{{ $i }}">Název (použit pro výsledky)</label>
                            </div>

                            @if ($i > 2)
                                <a
                                        class="btn-floating halfway-fab waves-effect waves-light deep-orange dropdown-button"
                                        data-toggle="dropdown"
                                        data-activates="card1-menu"
                                >
                                    <i class="material-icons">menu</i>
                                </a>

                                <ul id='card1-menu' class='dropdown-content'>
                                    <li><a href="#!">smazat</a></li>
                                </ul>
                            @endif
                        </div>

                        <div class="card-content brown lighten-4">
                            @component('shared.components.file-upload')
                            @slot('name', 'packages[' . $i . '][]')
                            @slot('multiple', true)
                            @slot('required', true)
                            @slot('description', 'Vložte jeden nebo více souborů')
                            @endcomponent
                        </div>
                    </div>
                </div>
            @endfor
        </div>

        <div class="row">
            <div class="input-field col s3 offset-s9">
                <input type="email" name="email" id="email" class="validate" required>
                <label for="email" data-error="E-mail není ve správném formátu">E-mail pro zaslání výsledků</label>
            </div>
        </div>
        <div class="row">
            <div class="col s3 offset-s9 right-align">
                <button
                    data-toggle="submit-loader"
                    data-text="Zpracovávám..."
                    class="waves-effect waves-light btn btn-large deep-orange"
                >
                    <i class="material-icons right">send</i>
                    Porovnat soubory
                </button>
            </div>
        </div>

    </form>
@stop