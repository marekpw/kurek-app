<!DOCTYPE html>
<html>
<head>
    <title>Porovnání souborů</title>
    <link href="{!! asset('css/app.css') !!}" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
    <nav class="brown darken-2" role="navigation">
        <div class="nav-wrapper container">
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
            <a id="logo-container" href="#" class="brand-logo brown-text text-lighten-4">Logo appky</a>
        </div>
    </nav>

    <ul id="slide-out" class="side-nav">
        <li>
            <div class="userView">
                <div class="background">
                    <img src="images/office.jpg">
                </div>
                <a href="#!user"><img class="circle" src="images/yuna.jpg"></a>
                <a href="#!name"><span class="white-text name">John Doe</span></a>
                <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
            </div>
        </li>
        <li><a href="#!"><i class="material-icons">cloud</i>First Link With Icon</a></li>
        <li><a href="#!">Second Link</a></li>
        <li><div class="divider"></div></li>
        <li><a class="subheader">Subheader</a></li>
        <li><a class="waves-effect" href="#!">Third Link With Waves</a></li>
    </ul>

    <div class="main-title">
        <h2>@yield('main-title', 'New Page')</h2>

        @if (View::hasSection('description'))
            <p>@yield('description')</p>
        @endif
    </div>

    <div class="section">
        <div class="container">
            @yield('content')
        </div>
    </div>

    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
</body>
</html>