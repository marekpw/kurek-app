<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ComparisonController@form')->name('comparison.form');
Route::post('/submit', 'ComparisonController@submit')->name('comparison.submit');
Route::get('/done', 'ComparisonController@done')->name('comparison.done');
Route::get('/results/{id}', 'ComparisonController@results')->where('id', '[a-zA-Z0-9]+')->name('comparison.results');