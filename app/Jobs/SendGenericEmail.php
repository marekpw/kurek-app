<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendGenericEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var \Illuminate\Contracts\Mail\Mailable
     */
    private $mail;
    /**
     * @var
     */
    private $to;

    /**
     * Create a new job instance.
     *
     * @param \Illuminate\Contracts\Mail\Mailable $mail
     * @param $to
     */
    public function __construct(Mailable $mail, $to)
    {
        $this->mail = $mail;
        $this->to = $to;

        $this->onQueue('default');
    }

    /**
     * Execute the job.
     *
     * @param \Illuminate\Contracts\Mail\Mailer $mailer
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->to)
            ->send($this->mail);
    }
}
