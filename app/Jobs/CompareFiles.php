<?php

namespace App\Jobs;

use App\Comparison;
use App\CsvComparison\FilePackage;

use App\Mail\ComparisonFinished;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Goodby\CSV\Export\Standard\Exporter;
use Goodby\CSV\Export\Standard\ExporterConfig;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\LexerConfig;

class CompareFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const TIMEOUT = 60*60; // 1 hour
    const MEMORY_LIMIT = '3G'; // 1 GB

    /**
     * @var \Illuminate\Support\Collection
     */
    private $packages;

    /**
     * @var Comparison
     */
    private $comparison;

    /**
     * Create a new job instance.
     *
     * @param $packages FilePackage[]|array|\Traversable
     * @param \App\Comparison $comparison
     */
    public function __construct($packages, Comparison $comparison)
    {
        $this->packages = collect($packages);
        $this->comparison = $comparison;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->comparison->start()->save();

        set_time_limit(static::TIMEOUT);
        ini_set('memory_limit', static::MEMORY_LIMIT);

        $progress = 0;

        $lexerConfig = new LexerConfig();
        $lexerConfig->setDelimiter(';');

        $lexer = new Lexer($lexerConfig);
        $output = collect();

        $this->packages->each(function(FilePackage $package, $key) use ($lexer, $output) {
            $headerRow = null;
            $headerIndexes = null;

            $interpreter = new Interpreter();
            $interpreter->addObserver(function(array $row) use (&$headerRow, &$headerIndexes, $output, $key) {
                if ($headerRow === null) {
                    $headerRow = $this->makeHeader($row);
                    $headerIndexes = array_flip(array_keys($headerRow));
                } else {
                    $row = array_combine($headerRow, array_intersect_key($row, $headerIndexes));

                    $price = $this->parseFloat($row['price']);

                    if (!$price || !$row['tecdoc_number'] || !$row['manufacturer']) {
                        return;
                    }

                    $uniqueCombination = $row['manufacturer'] . '---' . $row['tecdoc_number'];

                    if ($output->has($uniqueCombination)) {
                        $one = $output->get($uniqueCombination);
                        $one['price'][$key] = $price;
                        $one['item_number'][$key] = $row['item_number'];

                        if ($row['prefix']) {
                            if (!isset($one['prefix'])) {
                                $one['prefix'] = [];
                            }

                            $one['prefix'][$key] = $row['prefix'];
                        }

                        $output->put($uniqueCombination, $one);
                    } else {
                        $one = [
                            'manufacturer' => $row['manufacturer'],
                            'tecdoc' => $row['tecdoc_number'],
                            'price' => [
                                $key => $price
                            ],
                            'item_number' => [
                                $key => $row['item_number']
                            ]
                        ];

                        if ($row['prefix']) {
                            $one['prefix'] = [
                                $key => $row['prefix']
                            ];
                        }

                        $output->put($uniqueCombination, $one);
                    }
                }
            });

            $package->getFiles()->each(function($file) use ($lexer, $interpreter) {
                $lexer->parse($file, $interpreter);
            });
        });

        $resultsFile = $this->export($output);
        $this->comparison->finish($resultsFile)->save();

        $this->notify();
    }

    private function notify()
    {
        $mail = new ComparisonFinished($this->comparison);
        dispatch(new SendGenericEmail($mail, $this->comparison->email));
    }

    private function export($items)
    {
        $files = $this->packages;

        $packages = [];
        foreach ($files as $index => $file) {
            $packages[$index] = $file->getName();
        }

        $filename = "comparison/{$this->comparison->results_id}";

        $writer = WriterFactory::create('xlsx');
        $writer->openToFile(public_path("{$filename}.xlsx"));

        $header = [
            'Výrobce',
            'Číslo skup. Kat. číslo'
        ];

        foreach ($packages as $file) {
            $header[] = sprintf("Cena balíčku %s", $file);
        }

        $header[] = "Nejvýhodnější cena";
        $header[] = "Prefix u dodavatele";
        $header[] = "Objednací číslo nejvýhodnějšího dodavatele";
        $writer->addRow($header);

        foreach ($items as $key => $item) {
            $row = [
                $item['manufacturer'],
                $item['tecdoc']
            ];

            for ($i = 0; $i < count($packages); $i++) {
                $row[] = (isset($item['price'][$i]) && $item['price'][$i]) ? sprintf('%.2f', $item['price'][$i]) : 'N/A';
            }

            $cheapestIndex = array_search(min($item['price']), $item['price']);

            $row[] = $packages[$cheapestIndex] ?? 'N/A';
            $row[] = $item['prefix'][$cheapestIndex] ?? 'N/A';
            $row[] = $item['item_number'][$cheapestIndex] ?? 'N/A';

            $writer->addRow($row);
        }

        $writer->close();

        $resultsPath = public_path("{$filename}.zip");
        $zipper = \Zipper::make($resultsPath);
        $zipper->add(public_path("{$filename}.xlsx"));

        $zipper->close();

        \File::delete(public_path("{$filename}.xlsx"));


        return "{$filename}.zip";
    }


    /**
     * @param $row
     * @return array
     */
    private function makeHeader($row)
    {
        $translation = [
            'Prefix' => 'prefix',
            'Objednací číslo' => 'item_number',
            //'Výrobce/značka (u dodavatele)' => 'supplier_brand',
            //'Název (u dodavatele)' => 'name',
            'Nákupní cena' => 'price',
            'Min.odběr množství' => 'min_quantity',
            'Číslo skup. Kat. číslo' => 'tecdoc_number',
            'Výrobce' => 'manufacturer'
        ];

        $header = [];

        foreach ($row as $key => $item) {
            if ($key == 0) {
                $item = $this->removeBom($item);
            }

            if (!isset($translation[$item])) {
                continue;
            }

            $header[$key] = $translation[$item];
        }

        return $header;
    }

    private function removeBom($string)
    {
        $bom = pack('H*','EFBBBF');
        $string = preg_replace("/^$bom/", '', $string);

        return $string;
    }

    private function parseFloat($string)
    {
        return (float) str_replace(',', '.', $string);
    }
}
