<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2/15/2017
 * Time: 10:32 PM
 */

namespace App\CsvComparison;

use Illuminate\Support\Collection;

class FilePackage
{
    /**
     * @var Collection
     */
    private $files;

    /**
     * @var string
     */
    private $name;

    public function __construct($name = '', $files = [])
    {
        $this->setFiles($files);
        $this->setName($name);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param \Illuminate\Support\Collection|array|string $files
     * @return FilePackage
     */
    public function setFiles($files)
    {
        $this->files = new Collection($files);
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FilePackage
     */
    public function setName($name)
    {
        if (!$name) {
            $name = 'File Package';
        }

        $this->name = $name;
        return $this;
    }

    public function addFile($file)
    {
        $this->files->push($file);
        return $this;
    }
}