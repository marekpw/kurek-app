<?php

namespace App\Mail;

use App\Comparison;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComparisonFinished extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Comparison
     */
    private $comparison;

    public function __construct(Comparison $comparison)
    {
        $this->comparison = $comparison;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.comparison.finished', [
            'url' => url(route('comparison.results', ['id' => $this->comparison->results_id]))
        ]);
    }
}
