<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comparison extends Model
{
    const STATUS_WAITING = 'waiting';
    const STATUS_STARTED = 'started';
    const STATUS_FINISHED = 'finished';
    const STATUS_ERROR = 'error';

    protected $attributes = [
        'status' => self::STATUS_WAITING
    ];

    protected $fillable = [
        'step',
        'step_max',
        'status',
        'results_file',
        'email'
    ];

    public function __construct(array $attributes = [])
    {
        $this->attributes['results_id'] = str_random(4);

        parent::__construct($attributes);
    }

    /**
     * @param $file
     * @return \App\Comparison
     */
    public function finish($file)
    {
        $this->status = self::STATUS_FINISHED;
        $this->results_file = $file;

        return $this;
    }

    public function start()
    {
        $this->status = self::STATUS_STARTED;

        return $this;
    }

    public function error()
    {
        $this->status = self::STATUS_ERROR;

        return $this;
    }

    public function step($step)
    {
        if ($step > $this->step_max) {
            throw new \LogicException("Cannot increase step to a higher value than the max. Change the step_max field to a higher value");
        }

        $this->step = $step;

        return $this;
    }

    public function scopeFinished($query, $id)
    {
        return $query->where('status', self::STATUS_FINISHED)->where('results_id', $id);
    }
}
