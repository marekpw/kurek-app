<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2/2/2017
 * Time: 9:58 PM
 */

namespace App\Http\Controllers;

use App\Comparison;
use App\CsvComparison\FilePackage;
use App\Jobs\CompareFiles;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ComparisonController extends Controller
{
    public function form()
    {
        return view('comparison.form');
    }

    public function submit(Request $request)
    {
        $email = $request->input('email');

        $input = collect();

        $packages = $request->file('packages');
        $names = $request->input('package_names');

        foreach ($packages as $key => $package) {

            /**
             * some IDE assistance
             * @var $package UploadedFile[]
             */

            $filePackage = new FilePackage($names[$key]);

            foreach ($package as $file) {

                $filename = $file->getClientOriginalName();
                while (($uniqFilename = str_random() . "_{$filename}") && is_file(storage_path("csv/{$uniqFilename}")));

                $file->move(storage_path('csv'), $uniqFilename);
                $filePackage->addFile(storage_path("csv/{$uniqFilename}"));
            }

            $input->push($filePackage);
        }

        $comparison = new Comparison;
        $comparison->email = $email;
        $comparison->save();

        $job = new CompareFiles($input, $comparison);
        $job->onQueue('default');
        dispatch($job);

        return redirect()->route('comparison.done');
    }

    public function done()
    {
        return view('comparison.done');
    }

    public function results($id)
    {
        $comparison = Comparison::finished($id)->firstOrFail();

        return view('comparison.results', [
            'file' => url('/') . '/comparison/' . basename($comparison->results_file)
        ]);
    }
}