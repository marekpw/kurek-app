<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsvRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_number')->nullable();
            $table->string('supplier_brand')->nullable();
            $table->string('name')->nullable();
            $table->decimal('price', 10, 4);
            $table->decimal('min_quantity', 10, 4)->nullable();
            $table->string('manufacturer');
            $table->string('tecdoc_number');
            $table->uuid('uuid');

            $table->unique(['tecdoc_number', 'manufacturer', 'uuid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('csv_records');
    }
}
